# NeverSquare by Darkside

## Project

This game was create in the Sonen Game Jam (http://sonengamejam.org).
The challenge was to create a game in 24 hours.

Source code: https://bitbucket.org/robert_kolner/darkside

### Used libaries

* jQuery JavaScript library (version 2.0.3)
* Raphaël JavaScript vector library (version 2.1.2)
* Moment.js JavaScript date library (version 2.2.1)  

* jquery.timer.js jQuery plugin
* rhill-voronoi-core.js Voronoi JavaScript class